<?php

use App\Recipient;
use Faker\Generator as Faker;

$factory->define(Recipient::class, function (Faker $faker) {
    return [
        'name' =>  $faker->name,
        'email' => $faker->unique()->safeEmail,
    ];
});
