<?php

namespace Tests\Feature;

use App\Recipient;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RecipientTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Recipient repository interface
     *
     * @var App\Repositories\RecipientRepositoryInterface
     */
    private $recipient_repository;

    // recipient.index
    public function testIndex()
    {
        $this->get('recipient')
            ->assertViewHas('recipients')
            ->assertStatus(200);
    }

    // recipient.create
    public function testCreate()
    {
        $this->get('recipient/create')
            ->assertStatus(200);
    }

    // recipient.store
    public function testStore()
    {
        $data = [
            '_token' => csrf_token(),
            'name' => 'madson',
            'email' => 'madson@test.com',
        ];

        $this->post('recipient', $data)
            ->assertStatus(302)
            ->assertRedirect('recipient/madson');
    }

    // recipient.show
    public function testShow()
    {
        factory(Recipient::class)->create(['name' => 'Madson']);

        $recipient = Recipient::whereSlug('madson')->first();

        $this->get('recipient/madson')
            ->assertStatus(200)
            ->assertViewHas('recipient', $recipient);
    }

    // recipient.edit
    public function testEdit()
    {
        factory(Recipient::class)->create(['name' => 'Madson']);
        $recipient = Recipient::whereSlug('madson')->first();

        $this->get('recipient/madson/edit')
            ->assertStatus(200)
            ->assertViewHas('recipient', $recipient);
    }

    // recipient.update
    public function testUpdate()
    {
        $recipient = factory(Recipient::class)->create(['name' => 'Madson']);

        $data = [
            '_token' => csrf_token(),
            '_method' => 'PUT',
            'name' => 'Madson Verdi Junior',
            'email' => 'madson@test.com',
        ];

        $this->post('recipient/'.$recipient->id, $data)
            ->assertStatus(302)
            ->assertRedirect('recipient/madson-verdi-junior');
    }

    // recipient.destroy
    public function testDestroy()
    {
        $recipient = factory(Recipient::class)->create(['name' => 'Madson']);

        $data = [
            '_token' => csrf_token(),
            '_method' => 'DELETE',
        ];

        $this->post('recipient/'.$recipient->id, $data)
            ->assertStatus(302)
            ->assertRedirect('recipient');
    }
}
