@section('actionbar')
  <div class="card mb-3">
    <div class="card-header">
      <span class="text-muted">Action Bar</span>
      <div class="pull-right">

        @if (isset($create_route))
          {{-- Insert create button  --}}
          <a href="{{ route($create_route) }}" class="btn btn-secondary">Create</a>
        @endif

        <a href="#filterRecipient" class="btn btn-secondary" data-toggle="collapse" data-target="#filterRecipient">Show filters</a>
      </div>
    </div>
    <div class="collapse" id="filterRecipient">
      {{ Form::open(['route' => $form_route, 'method' => 'GET']) }}
        <div class="card-body">
          {{ $slot }}
        </div>
        <div class="card-footer text-right">
          <div class="btn-group">
            <a href="{{ Request::url() }}" class="btn btn-light">Clear filters</a>
            {{ Form::bsSubmit('Apply Filter') }}
          </div>
        </div>
      {{ Form::close() }}
    </div>
  </div>
@endsection
