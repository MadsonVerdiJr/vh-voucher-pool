
{{ Form::bsDynamicText($name, $label, [], ['class' => 'form-control form-phone']) }}

@push('js-helpers')
    <script>
        var phoneMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        phoneOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(phoneMaskBehavior.apply({}, arguments), options);
            }
        },
        applyPhoneMask = function () {
            $('.form-phone').mask(phoneMaskBehavior, phoneOptions);
        };

        $(document).on('mask-it', function(){
            applyPhoneMask();
        }).trigger('mask-it');

        $(document).on('click', '.btn-add', function(){
            $(this).trigger('mask-it');
        //     $('.form-phone').mask(phoneMaskBehavior, phoneOptions);
        //     console.log($('.form-phone'));
        });
    </script>
@endpush
