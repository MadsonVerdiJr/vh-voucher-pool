<div class="form-group @if($errors->has($name)) has-error @endif">
    {{ Form::label($name, $label, ['class' => 'control-label']) }}
    {{ Form::date($name, $value, array_merge(['class' => 'form-control'], $attributes)) }}
    {!! $errors->first($name, '<p class="help-block">:message</p>')  !!}
</div>
