<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} - @yield('title')</title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="#">Navbar</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
          <li class="nav-item active">
            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Features</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Pricing</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Dropdown link
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="#">Action</a>
              <a class="dropdown-item" href="#">Another action</a>
              <a class="dropdown-item" href="#">Something else here</a>
            </div>
          </li>
        </ul>
      </div>
    </nav>

    <div class="container">
      <h1>@yield('title', config('app.name', 'Laravel'))</h1>

      <h2 class="border-bottom">Form example</h2>

      {{ Form::open() }}

        {{ Form::bsCheckbox('teste') }}

        {{ Form::bsCnpj('cnpj', 'CNPJ') }}

        {{ Form::bsCpf('cpf') }}

        {{ Form::bsDate('Date') }}

        {{ Form::bsDatetime('Datetime') }}

        {{ Form::bsDynamicText('dynamic_text') }}

        {{ Form::bsEmail('email') }}

        {{ Form::bsPassword('password') }}

        {{ Form::bsNumber('number') }}

        {{ Form::bsPhone('phone') }}

        {{ Form::bsPhones('phones') }}

        {{ Form::bsRg('rg') }}

        {{ Form::bsSelect('select') }}

        {{ Form::bsText('text') }}

        {{ Form::bsTextarea('textarea') }}

        {{ Form::bsSubmit('submit') }}
      {{ Form::close() }}

    </div>

    <script src="{{ asset('js/app.js') }}"></script>

    @stack('js-helpers')
  </body>
</html>
