@extends('layouts.app')

@section('content')

  {{ Breadcrumbs::render('special-offer.show', $special_offer) }}

  <div class="row">
    <div class="col-md-4">
      @include('special-offer.component.card')
    </div>
    <div class="col-md-8 mb-3">
      <div class="card">
        <h5 class="card-header">Recipients</h5>
        @include('recipient.component.table')
      </div>
    </div>
    <div class="col-md-8 offset-4">
      <div class="card">
        <div class="card-header"></div>
      </div>
    </div>
  </div>


@endsection

@include('components.modal.delete', ['action' => route('special-offer.destroy', $special_offer->id), 'modal_name' => 'deleteRecipient'])

