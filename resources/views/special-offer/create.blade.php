@extends('layouts.app')

@section('content')

  {{ Breadcrumbs::render('special-offer.create') }}

  {{ Form::open(['route' => 'special-offer.store', 'method' => 'POST']) }}
    <div class="card">
      <div class="card-body">
        @include('special-offer.component.form')
      </div>
      <div class="card-footer text-right">
        {{ Form::bsSubmit('Create') }}
      </div>
    </div>
  {{ Form::close() }}

@endsection

