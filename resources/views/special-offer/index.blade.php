@extends('layouts.app')

@section('content')

  {{ Breadcrumbs::render('special-offer.index') }}

  @include('special-offer.component.filter')

  <div class="card">
    @include('special-offer.component.table')
  </div>


@endsection

