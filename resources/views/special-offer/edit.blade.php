@extends('layouts.app')

@section('content')

  {{ Breadcrumbs::render('special-offer.edit', $special_offer) }}

  {{ Form::open(['route' => ['special-offer.update', $special_offer->id], 'method' => 'PUT']) }}
    <div class="card">
      <div class="card-body">
        @include('special-offer.component.form')
      </div>
      <div class="card-footer text-right">
        {{ Form::bsSubmit('Update') }}
      </div>
    </div>
  {{ Form::close() }}

@endsection

