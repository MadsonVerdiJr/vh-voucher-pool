<table class="table table-hover">
  <thead class="thead-light">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Discount</th>
      <th scope="col">Validity</th>
      <th scope="col">Actions</th>
    </tr>
  </thead>
  <tbody>
    @forelse($special_offers as $special_offer)
      <tr>
        <th scope="row">{{ $special_offer->id }}</th>
        <td>{{ $special_offer->name }}</td>
        <td>{{ $special_offer->discount }}%</td>
        <td>{{ $special_offer->validity }}%</td>
        <td>
          <div class="btn-group" role="group" aria-label="Actions for special_offer">
            <a href="{{ route('special-offer.show', $special_offer->slug) }}" class="btn btn-secondary">Show</a>
          </div>
        </td>
      </tr>
    @empty
      <tr>
        <td colspan="5">No Special Offers registered</td>
      </tr>
    @endforelse
  </tbody>

  @if ($special_offers instanceof \Illuminate\Pagination\Paginator and $special_offers->hasPages())
      <tfoot>
          <tr>
              <td colspan="5">
                  <div class="justify-content-center">

                  {{ $special_offers->appends(Request::capture()->except('page'))->links() }}
                  </div>
              </td>
          </tr>
      </tfoot>
  @endif
</table>
