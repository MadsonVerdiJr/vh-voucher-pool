{{ Form::bsText('name', null, isset($special_offer) ? $special_offer->name : null) }}

{{ Form::bsNumber('discount', null, isset($special_offer) ? $special_offer->discount : null) }}

{{ Form::bsDate('validity', null, isset($special_offer) ? $special_offer->validity : null) }}
