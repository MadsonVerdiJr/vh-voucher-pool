<div class="card">
  <div class="card-body">
    <h5 class="card-title">{{ $special_offer->name }}</h5>
  </div>
  <div class="list-group list-group-flush">
    <div class="list-group-item list-group-item-action">
      Discount: {{ $special_offer->discount }}%
    </div>
    <div class="list-group-item list-group-item-action">
      Validity: {{ $special_offer->validity }}
    </div>
  </div>
  <div class="card-footer text-right">
    <div class="btn-group ">
        <a href="#" class="btn btn-secondary" data-toggle="modal" data-target="#deleteRecipient">Delete</a>
        <a href="{{ route('special-offer.edit', $special_offer->slug) }}" class="btn btn-secondary">Edit</a>
      </div>
  </div>
</div>
