@extends('layouts.app')

@section('content')

  {{ Breadcrumbs::render('recipient.index') }}

  <div class="row">
    <div class="col-12">
      @include('recipient.component.filter')
    </div>
  </div>

  <div class="card">

    @include('recipient.component.table')

  </div>


@endsection

