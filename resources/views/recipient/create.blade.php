@extends('layouts.app')

@section('content')

  {{ Breadcrumbs::render('recipient.create') }}

  {{ Form::open(['route' => 'recipient.store', 'method' => 'POST']) }}
    <div class="card">
      <div class="card-body">
        @include('recipient.component.form')
      </div>
      <div class="card-footer text-right">
        {{ Form::bsSubmit('Create') }}
      </div>
    </div>
  {{ Form::close() }}

@endsection

