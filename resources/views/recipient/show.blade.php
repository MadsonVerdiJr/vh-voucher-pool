@extends('layouts.app')

@section('content')

  {{ Breadcrumbs::render('recipient.show', $recipient) }}

  <div class="row">

    <div class="col-4">
      @component('recipient.component.card', ['recipient' => $recipient])
      @endcomponent
    </div>

    <div class="col-8">
      <div class="card">
        <div class="card-header">
          <h5 class="card-title">Voucher Codes</h5>
        </div>
        @component('voucher-code.component.table', ['voucher_codes' => $voucher_codes])
        @endcomponent
      </div>
    </div>

    <div class="mt-3 col-8 offset-4">
      <div class="card">
        <div class="card-header">
          <h5 class="card-title">Special Offers</h5>
        </div>
        @component('special-offer.component.table', ['special_offers' => $special_offers])
        @endcomponent
      </div>
    </div>
  </div>


@endsection

@include('components.modal.delete', ['action' => route('recipient.destroy', $recipient->id), 'modal_name' => 'deleteRecipient'])

