<table class="table table-hover">
  <thead class="thead-light">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Email</th>
      <th scope="col">Actions</th>
    </tr>
  </thead>
  <tbody>
    @forelse($recipients as $recipient)
      <tr>
        <th scope="row">{{ $recipient->id }}</th>
        <td>{{ $recipient->name }}</td>
        <td>{{ $recipient->email }}</td>
        <td>
          <div class="btn-group" role="group" aria-label="Actions for recipient">
            <a href="{{ route('recipient.show', $recipient->slug) }}" class="btn btn-secondary">Show</a>
          </div>
        </td>
      </tr>
    @empty
      <tr>
        <td colspan="4">No recipients registered</td>
      </tr>
    @endforelse
  </tbody>

  @if ($recipients instanceof \Illuminate\Pagination\Paginator and $recipients->hasPages())
      <tfoot>
          <tr>
              <td colspan="6">
                  <div class="justify-content-center">

                  {{ $recipients->appends(Request::capture()->except('page'))->links() }}
                  </div>
              </td>
          </tr>
      </tfoot>
  @endif
</table>
