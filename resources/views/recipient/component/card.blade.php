<div class="card">
  <div class="card-body">
    <h5 class="card-title">{{ $recipient->name }}</h5>
    <h6 class="card-subtitle mb-2 text-muted">{{ $recipient->email }}</h6>
  </div>
  <div class="card-footer text-right">
    <div class="btn-group ">
        <a href="#" class="btn btn-secondary" data-toggle="modal" data-target="#deleteRecipient">Delete</a>
        <a href="{{ route('recipient.edit', $recipient->slug) }}" class="btn btn-secondary">Edit</a>
      </div>
  </div>
</div>
