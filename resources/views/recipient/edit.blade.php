@extends('layouts.app')

@section('content')

  {{ Breadcrumbs::render('recipient.edit', $recipient) }}

  {{ Form::open(['route' => ['recipient.update', $recipient->id], 'method' => 'PUT']) }}
    <div class="card">
      <div class="card-body">
        @include('recipient.component.form')
      </div>
      <div class="card-footer text-right">
        {{ Form::bsSubmit('Update') }}
      </div>
    </div>
  {{ Form::close() }}

@endsection

