<div class="card">
  <div class="card-body">
    <h5 class="card-title">{{ $voucher_code->code }}</h5>
  </div>
  <div class="card-footer text-right">
    <div class="btn-group ">
        <a href="{{ route('voucher-code.redeem', $voucher_code->code) }}" class="btn btn-secondary">Redeem</a>
      </div>
  </div>
</div>
