@component('components.actionbar', ['form_route' => 'special-offer.index'])

  {{ Form::bsText('voucher-code', 'Voucher Code') }}

  <h5>Recipient</h5>
  <hr>

  {{ Form::bsText('recipient-name', 'Name') }}

  {{ Form::bsText('recipient-email', 'E-mail') }}
  <h5>Special Offer</h5>
  <hr>
  {{ Form::bsText('special-offer-name', 'Name') }}

  {{ Form::bsNumber('special-offer-discount', 'Discount grather than') }}

  {{ Form::bsText('special-offer-validity', 'Validity') }}

@endcomponent
