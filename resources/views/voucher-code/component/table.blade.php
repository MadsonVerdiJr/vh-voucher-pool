<table class="table table-hover">
  <thead class="thead-light">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Recipient</th>
      <th scope="col">Special Offer</th>
      <th scope="col">Validity</th>
      <th scope="col">Actions</th>
    </tr>
  </thead>
  <tbody>
    @forelse($voucher_codes as $voucher_code)
      <tr>
        <th scope="row">{{ $voucher_code->code }}</th>
        <td>
          <p class="text-uppercase">
            {{ $voucher_code->recipient->name }}
          </p>
          <small class="text-muted">{{ $voucher_code->recipient->email }}</small>
        </td>
        <td>
          {{ $voucher_code->specialOffer->name }}
        </td>

        <td>
          {{ $voucher_code->specialOffer->validity }}
        </td>
        <td>
          <div class="btn-group" role="group" aria-label="Actions for recipient">
            <a href="{{ route('voucher-code.show', $voucher_code->code) }}" class="btn btn-secondary">Show</a>
          </div>
        </td>
      </tr>
    @empty
      <tr>
        <td colspan="5">No Voucher Codes registered</td>
      </tr>
    @endforelse
  </tbody>

  @if ($voucher_codes instanceof \Illuminate\Pagination\Paginator and $voucher_codes->hasPages())
      <tfoot>
          <tr>
              <td colspan="5">
                  <div class="justify-content-center">

                  {{ $voucher_codes->appends(Request::capture()->except('page'))->links() }}
                  </div>
              </td>
          </tr>
      </tfoot>
  @endif
</table>
