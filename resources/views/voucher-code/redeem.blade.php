@extends('layouts.app')

@section('content')

  {{ Breadcrumbs::render('voucher-code.redeem', $voucher_code) }}

  <div class="card">
    {{ Form::open(['route' => ['voucher-code.redeem', $voucher_code->code], 'method' => 'POST']) }}

      <div class="card-body">
        {{ Form::bsEmail('email', 'Confirm e-mail') }}
      </div>

      <div class="card-footer text-right">
        {{ Form::bsSubmit('Redeem') }}
      </div>

    {{ Form::close() }}

  </div>


@endsection

