@extends('layouts.app')

@section('content')

  {{ Breadcrumbs::render('voucher-code.index') }}

  @include('voucher-code.component.filter')

  <div class="card">

    @include('voucher-code.component.table')

  </div>


@endsection

