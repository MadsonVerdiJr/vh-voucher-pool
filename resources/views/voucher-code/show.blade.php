@extends('layouts.app')

@section('content')

  {{ Breadcrumbs::render('voucher-code.show', $voucher_code) }}

  <div class="row">
    <div class="col-md-4">
      @include('voucher-code.component.card')
    </div>
    <div class="col-md-8 mb-3">
      <div class="card">
        <h5 class="card-header">Recipient</h5>
        @include('recipient.component.card', ['recipient' => $voucher_code->recipient])
      </div>
    </div>
    <div class="col-md-8 offset-4">
      <div class="card">
        <div class="card-header">Special Offer</div>
        @include('special-offer.component.card', ['special_offer' => $voucher_code->specialOffer])
      </div>
    </div>
  </div>

@endsection
