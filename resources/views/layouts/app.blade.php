<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} - @yield('title')</title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
      <a class="navbar-brand" href="{{ route('home') }}">Navbar</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
          <li class="nav-item dropdown">
            <a class="nav-link" href="{{ route('recipient.index') }}">Recipient</a>
          </li>

          <li class="nav-item dropdown">
            <a class="nav-link" href="{{ route('special-offer.index') }}">Special Offer</a>
          </li>

          <li class="nav-item dropdown">
            <a class="nav-link" href="{{ route('voucher-code.index') }}">Voucher Code</a>
          </li>
        </ul>
      </div>
    </nav>

    <div class="container">
      <div class="row">
        <div class="col-4">
          @yield('breadcrumb')
        </div>
        <div class="col-8">
          @yield('actionbar')
        </div>
      </div>

      @yield('content')

    </div>

    <script src="{{ asset('js/app.js') }}"></script>

    @stack('js-helpers')
    @stack('modal')
  </body>
</html>
