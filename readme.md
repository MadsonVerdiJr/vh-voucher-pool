# VH - Voucher Pool

This PHP based system provide a control panel for manage **Recipients (Clients)**, **Vouchers** and **Offers**. This system was separated into two, a WEB System and a API.

## Implementation

This system was implemented with [Laravel 5.6](https://laravel.com) (A PHP framework).

## Functionalities

- WEB System
    + Manage Recipients
    + Manage Special Offers
    + View Vouchers Codes
- API System
    + View Recipients
        * View Vouchers codes from recipient
        * View Special Offers from recipient
    + Redeem Codes

## Tests

Some implemented tests?

- API
    + Postman collection: `vh-voucher-pool.postman_collection.json`
- PHPUNIT
    + Recipient Routes


## Version Control

- v0.1.0
