<?php

namespace App\Repositories;

interface RecipientRepositoryInterface
{
    /**
     * Create a recipient
     *
     * @param  string $name  - Recipient Name
     * @param  string $email - Recipient E-mail
     *
     * @return App\Recipient
     */
    public function create($name, $email);

    /**
     * Delete a recipient
     *
     * @param  integer $id - Recipient ID
     *
     * @return void
     */
    public function delete($id);

    /**
     * Get all recipients from the system
     *
     * @param  array  $filters - Array of filters
     *
     * @return Collection of \App\Recipient
     */
    public function getAll($filters = []);

    /**
     * Get all categories paginated
     *
     * @param  array  $filters - Filters used for pagination
     *
     * @return Illuminate\Pagination\Paginator
     */
    public function getAllPaginated($filters = [], $perPage = 15);

    /**
     * Get recipient by his ID
     *
     * @param  integer $id - Recipient ID
     *
     * @return App\Recipient
     */
    public function getById($id);

    /**
     * Get recipient by his E-mail
     *
     * @param  string $email - E-mail address
     *
     * @return \App\Recipient
     */
    public function getByEmail($email);

    /**
     * Get recipient by his SLUG
     *
     * @param  string $slug - Recipient slug
     *
     * @return App\Recipient
     */
    public function getBySlug($slug);

    /**
     * Update a recipient
     *
     * @param  integer $id    - Recipient ID
     * @param  string  $name  - Recipient Name
     * @param  string  $email - Recipient E-mail
     *
     * @return void
     */
    public function update($id, $name, $email);
}
