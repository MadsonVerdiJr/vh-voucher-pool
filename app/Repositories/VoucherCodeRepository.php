<?php

namespace App\Repositories;

use App\VoucherCode;
use Carbon\Carbon;

class VoucherCodeRepository implements VoucherCodeRepositoryInterface
{
    /**
     * Voucher Code Model
     *
     * @var \App\VoucherCode
     */
    private $voucher_code_model;

    /**
     * Apply filters in model
     *
     * @param  array  $filters - Array of filters
     *
     * @return \App\Recipient
     */
    private function applyFilters(array $filters)
    {
        if (isset($filters['recipient_email'])) {
            $this->voucher_code_model = $this->voucher_code_model->filterEmail($filters['recipient_email']);
        }
        return $this->voucher_code_model;
    }

    /**
     * Class constructor
     *
     * @param \App\VoucherCode $voucher_code_model - Voucher Code model
     */
    public function __construct(VoucherCode $voucher_code_model)
    {
        $this->voucher_code_model = $voucher_code_model;
    }

    /**
     * Check owner of Voucher Code by his e-mail
     *
     * @param  string $code  - Voucher Code
     * @param  string $email - Recipient e-mail
     *
     * @return Boolean
     */
    public function checkOwnerByEmail($code, $email)
    {
        $voucher = $this->voucher_code_model->whereCode($code)->filterEmail($email)->first();

        return $voucher !== null;
    }

    /**
     * Create Voucher Code
     *
     * @param  \App\Recipient    $recipient     - Recipient Model
     * @param  \App\SpecialOffer $special_offer - Special Offer Model
     *
     * @return \App\VoucherCode
     */
    public function create(\App\Recipient $recipient, \App\SpecialOffer $special_offer)
    {
        return $this->voucher_code_model->create([
            'code' => str_random(16),
            'recipient_id' => $recipient->id,
            'special_offer_id' => $special_offer->id,
        ]);
    }

    /**
     * Delete a Voucher Code
     *
     * @param string $code - Voucher Code identifier code
     *
     * @return void
     */
    public function delete($code)
    {
    }

    /**
     * Get all Voucher Codes
     *
     * @param  array  $filters - Array of filters
     *
     * @return \App\VoucherCode
     */
    public function getAll(array $filters = [])
    {
        $this->applyFilters($filters);

        return $this->voucher_code_model->get();
    }

    /**
     * Get all categories paginated
     *
     * @param  array    $filters - Filters used for pagination
     * @param  integer  $perPage - Amount of registers per page
     *
     * @return Illuminate\Pagination\Paginator
     */
    public function getAllPaginated($filters = [], $perPage = 15)
    {
        $this->applyFilters($filters);

        return $this->voucher_code_model
            ->paginate($perPage);
    }

    /**
     * Get Latest Voucher Codes from a Recipient
     *
     * @param  \App\Recipient $recipient - Recipient Model
     * @param  integer        $limit     - Limit of registers
     *
     * @return Illuminate\Pagination\Paginator
     */
    public function getLatestFromRecipient(\App\Recipient $recipient, $limit = 10)
    {
        return $this->voucher_code_model
            ->whereRecipientId($recipient->id)
            ->limit($limit)
            ->get();
    }

    /**
     * Get Voucher Code by his code
     *
     * @param string $code - Voucher Code identifier code
     *
     * @return App\VoucherCode
     */
    public function getByCode($code)
    {
        return $this->voucher_code_model->whereCode($code)->first();
    }

    /**
     * Set a Voucher Code as Used
     *
     * @param string $code - Voucher Code identifier code
     */
    public function setUsed($code)
    {
        $this->voucher_code_model
            ->whereCode($code)
            ->whereNull('used')
            ->update(['used' => Carbon::now(),]);

        $voucher = $this->voucher_code_model->whereCode($code)->first();

        return $voucher;
    }
}
