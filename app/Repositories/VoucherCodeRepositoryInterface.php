<?php

namespace App\Repositories;

interface VoucherCodeRepositoryInterface
{
    /**
     * Check owner of Voucher Code by his e-mail
     *
     * @param  string $code  - Voucher Code
     * @param  string $email - Recipient e-mail
     *
     * @return Boolean
     */
    public function checkOwnerByEmail($code, $email);

    /**
     * Create Voucher Code
     *
     * @param  \App\Recipient    $recipient     - Recipient Model
     * @param  \App\SpecialOffer $special_offer - Special Offer Model
     *
     * @return \App\VoucherCode
     */
    public function create(\App\Recipient $recipient, \App\SpecialOffer $special_offer);

    /**
     * Delete a Voucher Code
     *
     * @param string $code - Voucher Code identifier code
     *
     * @return void
     */
    public function delete($code);

    /**
     * Get all Voucher Codes
     *
     * @param  array  $filters - Array of filters
     *
     * @return \App\VoucherCode
     */
    public function getAll(array $filters = []);

    /**
     * Get all categories paginated
     *
     * @param  array    $filters - Filters used for pagination
     * @param  integer  $perPage - Amount of registers per page
     *
     * @return Illuminate\Pagination\Paginator
     */
    public function getAllPaginated($filters = [], $perPage = 15);

    /**
     * Get Latest Voucher Codes from a Recipient
     *
     * @param  \App\Recipient $recipient - Recipient Model
     * @param  integer        $limit     - Limit of registers
     *
     * @return Illuminate\Pagination\Paginator
     */
    public function getLatestFromRecipient(\App\Recipient $recipient, $limit = 10);

    /**
     * Get Voucher Code by his code
     *
     * @param string $code - Voucher Code identifier code
     *
     * @return App\VoucherCode
     */
    public function getByCode($code);

    /**
     * Set a Voucher Code as Used
     *
     * @param string $code - Voucher Code identifier code
     */
    public function setUsed($code);
}
