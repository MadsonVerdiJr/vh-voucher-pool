<?php

namespace App\Repositories;

interface SpecialOfferRepositoryInterface
{
    /**
     * Create Special Offer
     *
     * @param  string        $name     - Special Offer name
     * @param  integer       $discount - Special Offer discount
     * @param  Carbon\Carbon $validity - Special Offer validity date
     *
     * @return App\SpecialOffer
     */
    public function create($name, $discount, \Carbon\Carbon $validity);

    /**
     * Delete a recipient
     *
     * @param  integer $id - Recipient ID
     *
     * @return void
     */
    public function delete($id);

    /**
     * Get latest Special Offers from a Recipient
     *
     * @param  \App\Recipient $recipient - Recipient model
     * @param  integer        $limit     - Limit of registers
     *
     * @return Illuminate\Pagination\Paginator
     */
    public function getLatestFromRecipient(\App\Recipient $recipient, $limit = 10);

    /**
     * Get all categories paginated
     *
     * @param  array  $filters - Filters used for pagination
     *
     * @return Illuminate\Pagination\Paginator
     */
    public function getAllPaginated(array $filters = [], $perPage = 15);

    /**
     * Get recipient by his ID
     *
     * @param  integer $id - Recipient ID
     *
     * @return App\SpecialOffer
     */
    public function getById($id);

    /**
     * Get recipient by his SLUG
     *
     * @param  string $slug - Recipient slug
     *
     * @return App\SpecialOffer
     */
    public function getBySlug($slug);

    /**
     * Create Special Offer
     *
     * @param  integer       $id       - Special Offer ID
     * @param  string        $name     - Special Offer name
     * @param  integer       $discount - Special Offer discount
     * @param  Carbon\Carbon $validity - Special Offer validity date
     *
     * @return App\SpecialOffer
     */
    public function update($id, $name, $discount, \Carbon\Carbon $validity);
}
