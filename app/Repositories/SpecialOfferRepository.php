<?php

namespace App\Repositories;

use App\SpecialOffer;
use App\Repositories\RecipientRepositoryInterface;
use App\Repositories\VoucherCodeRepositoryInterface;

class SpecialOfferRepository implements SpecialOfferRepositoryInterface
{
    /**
     * Recipient repository interface
     *
     * @var \App\Repositories\RecipientRepositoryInterface
     */
    private $recipient_repository;

    /**
     * Special Offer model
     *
     * @var App\SpecialOffer
     */
    private $special_offer_model;

    /**
     * Voucher Code repository interface
     *
     * @var \App\Repositories\VoucherCodeRepositoryInterface
     */
    private $voucher_code_repository;

    /**
     * Apply filters in model
     *
     * @param  array  $filters - Array of Filters
     *
     * @return \App\SpecialOffer
     */
    private function applyFilter(array $filters)
    {
        if (isset($filters['special_offer'])) {
            $this->special_offer_model = $this->special_offer_model->whereSlug($special_offer_model);
        }
    }

    /**
     * Class constructor
     *
     * @param SpecialOffer                   $special_offer_model     - Special Offer model
     * @param VoucherCodeRepositoryInterface $voucher_code_repository - Voucher Code repository interface
     */
    public function __construct(
        RecipientRepositoryInterface $recipient_repository,
        SpecialOffer $special_offer_model,
        VoucherCodeRepositoryInterface $voucher_code_repository
    ) {
        // Model
        $this->special_offer_model = $special_offer_model;

        // Repositories
        $this->recipient_repository = $recipient_repository;
        $this->voucher_code_repository = $voucher_code_repository;
    }

    /**
     * Create Special Offer
     *
     * @param  string        $name     - Special Offer name
     * @param  integer       $discount - Special Offer discount
     * @param  Carbon\Carbon $validity - Special Offer validity date
     *
     * @return App\SpecialOffer
     */
    public function create($name, $discount, \Carbon\Carbon $validity)
    {
        $this->special_offer_model = $this->special_offer_model->create([
            'name' => $name,
            'discount' => $discount,
            'validity' => $validity,
        ]);

        foreach ($this->recipient_repository->getAll() as $recipient) {
            $this->voucher_code_repository->create($recipient, $this->special_offer_model);
        }

        return $this->special_offer_model;

    }

    /**
     * Delete a recipient
     *
     * @param  integer $id - Recipient ID
     *
     * @return void
     */
    public function delete($id)
    {
        $this->special_offer_model->destroy($id);
    }

    /**
     * Get latest Special Offers from a Recipient
     *
     * @param  \App\Recipient $recipient - Recipient model
     * @param  integer        $limit     - Limit of registers
     *
     * @return Illuminate\Pagination\Paginator
     */
    public function getLatestFromRecipient(\App\Recipient $recipient, $limit = 10)
    {
        return $this->special_offer_model
            ->whereHas('recipients', function ($query) use ($recipient) {
                $query->whereId($recipient->id);
            })
            ->paginate($limit+1);
    }

    /**
     * Get all categories paginated
     *
     * @param  array  $filters - Filters used for pagination
     *
     * @return Illuminate\Pagination\Paginator
     */
    public function getAllPaginated(array $filters = [], $perPage = 15)
    {
        $this->applyFilter($filters);

        return $this->special_offer_model->paginate($perPage);
    }

    /**
     * Get recipient by his ID
     *
     * @param  integer $id - Recipient ID
     *
     * @return App\SpecialOffer
     */
    public function getById($id)
    {
    }

    /**
     * Get recipient by his SLUG
     *
     * @param  string $slug - Recipient slug
     *
     * @return App\SpecialOffer
     */
    public function getBySlug($slug)
    {
        return $this->special_offer_model->whereSlug($slug)->first();
    }

    /**
     * Create Special Offer
     *
     * @param  integer       $id       - Special Offer ID
     * @param  string        $name     - Special Offer name
     * @param  integer       $discount - Special Offer discount
     * @param  Carbon\Carbon $validity - Special Offer validity date
     *
     * @return App\SpecialOffer
     */
    public function update($id, $name, $discount, \Carbon\Carbon $validity)
    {
        $this->special_offer_model->findOrFail($id)->update([
            'name' => $name,
            'discount' => $discount,
            'validity' => $validity,
        ]);

        $this->special_offer_model = $this->special_offer_model->findOrFail($id);

        return $this->special_offer_model;
    }
}
