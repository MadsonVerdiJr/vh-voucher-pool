<?php

namespace App\Repositories;

use App\Recipient;

class RecipientRepository implements RecipientRepositoryInterface
{
    /**
     * Recipient Model
     *
     * @var App\Recipient
     */
    private $recipient_model;

    /**
     * Apply filters in model
     *
     * @param  array  $filters - Array of filters
     *
     * @return \App\Recipient
     */
    private function applyFilters(array $filters)
    {
        if (isset($filters['special_offer_slug'])) {
            $this->recipient_model = $this->recipient_model->filterSpecialOffer($filters['special_offer_slug']);
        }

        if (isset($filters['recipient_name'])) {
            $this->recipient_model = $this->recipient_model->filterName($filters['recipient_name']);
        }

        if (isset($filters['recipient_email'])) {
            $this->recipient_model = $this->recipient_model->filterEmail($filters['recipient_email']);
        }

        return $this->recipient_model;
    }


    /**
     * Class constructor
     *
     * @param App\Recipient $recipient_model
     */
    public function __construct(Recipient $recipient_model)
    {
        $this->recipient_model = $recipient_model;
    }

    /**
     * Create a recipient
     *
     * @param  string $name  - Recipient Name
     * @param  string $email - Recipient E-mail
     *
     * @return App\Recipient
     */
    public function create($name, $email)
    {
        $this->recipient_model = $this->recipient_model->create([
            'name' => $name,
            'email' => $email,
        ]);

        return $this->recipient_model;
    }

    /**
     * Delete a recipient
     *
     * @param  integer $id - Recipient ID
     *
     * @return void
     */
    public function delete($id)
    {
        $this->recipient_model->destroy($id);
    }

    /**
     * Get all recipients from the system
     *
     * @param  array  $filters - Array of filters
     *
     * @return Collection of \App\Recipient
     */
    public function getAll($filters = [])
    {
        $this->applyFilters($filters);

        return $this->recipient_model
            ->orderBy('name')
            ->get();
    }

    /**
     * Get all categories paginated
     *
     * @param  array  $filters - Filters used for pagination
     *
     * @return Illuminate\Pagination\Paginator
     */
    public function getAllPaginated($filters = [], $perPage = 15)
    {
        $this->applyFilters($filters);

        return $this->recipient_model
            ->orderBy('name')
            ->paginate($perPage);
    }

    /**
     * Get recipient by his ID
     *
     * @param  integer $id - Recipient ID
     *
     * @return App\Recipient
     */
    public function getById($id)
    {
        return $this->recipient_model->findOrFail($id);
    }

    /**
     * Get recipient by his E-mail
     *
     * @param  string $email - E-mail address
     *
     * @return \App\Recipient
     */
    public function getByEmail($email)
    {
        return $this->recipient_model->whereEmail($email)->first();
    }

    /**
     * Get recipient by his SLUG
     *
     * @param  string $slug - Recipient slug
     *
     * @return App\Recipient
     */
    public function getBySlug($slug)
    {
        return $this->recipient_model->whereSlug($slug)->first();
    }

    /**
     * Update a recipient
     *
     * @param  integer $id    - Recipient ID
     * @param  string  $name  - Recipient Name
     * @param  string  $email - Recipient E-mail
     *
     * @return void
     */
    public function update($id, $name, $email)
    {
        $this->recipient_model->findOrFail($id)->update([
            'name' => $name,
            'email' => $email,
        ]);

        $this->recipient_model = $this->recipient_model->findOrFail($id);

        return $this->recipient_model;
    }
}
