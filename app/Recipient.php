<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Recipient extends Model
{
    use Sluggable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ],
        ];
    }

    /**
     * Special Offer relation with Recipient model
     * @return \App\SpecialOffer
     */
    public function specialOffers(){
        return $this->belongsToMany(
            SpecialOffer::class,
            'voucher_codes',
            'recipient_id',
            'special_offer_id'
        );
    }

    /**
     * Apply E-mail filter in model
     *
     * @param  \Illuminate\Database\Eloquent\Builde $query
     * @param  string                               $email
     *
     * @return \Illuminate\Database\Eloquent\Builde
     */
    public function scopeFilterEmail($query, $email)
    {
        return $query->where('email', 'like', '%'.$email.'%');
    }

    /**
     * Apply Name filter in model
     *
     * @param  \Illuminate\Database\Eloquent\Builde $query
     * @param  string                               $name
     *
     * @return \Illuminate\Database\Eloquent\Builde
     */
    public function scopeFilterName($query, $name)
    {
        return $query->where('name', 'like', '%'.$name.'%');
    }


    /**
     * Apply Special offer filter in model
     *
     * @param  \Illuminate\Database\Eloquent\Builde $query
     * @param  string                               $name
     *
     * @return \Illuminate\Database\Eloquent\Builde
     */
    public function scopeFilterSpecialOffer($query, $slug)
    {
        return $query->whereHas('specialOffers', function ($special_offer) use ($slug) {
            $special_offer->whereSlug($slug);
        }) ;
    }
}
