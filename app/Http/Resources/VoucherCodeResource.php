<?php

namespace App\Http\Resources;

use App\Http\Resources\SpecialOfferResource;
use Illuminate\Http\Resources\Json\JsonResource;

class VoucherCodeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'code' => $this->code,
            'used' => $this->used,
            'offer' => new SpecialOfferResource($this->specialOffer),
        ];
    }
}
