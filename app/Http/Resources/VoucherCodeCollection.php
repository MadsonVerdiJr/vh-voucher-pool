<?php

namespace App\Http\Resources;

use App\Http\Resources\VoucherCodeResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class VoucherCodeCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => VoucherCodeResource::collection($this->collection)
        ];
    }
}
