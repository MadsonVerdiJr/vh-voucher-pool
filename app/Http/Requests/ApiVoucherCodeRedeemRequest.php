<?php

namespace App\Http\Requests;

use App\Repositories\VoucherCodeRepositoryInterface;
use Illuminate\Foundation\Http\FormRequest;

class ApiVoucherCodeRedeemRequest extends FormRequest
{
    /**
     * Voucher Code repository interface
     *
     * @var \App\Repositories\VoucherCodeRepositoryInterface
     */
    private $voucher_code_repository;

    /**
     * Class constructor
     *
     * @param \App\Repositories\VoucherCodeRepositoryInterface $voucher_code_repository - Voucher Code repository interface
     */
    public function __construct(VoucherCodeRepositoryInterface $voucher_code_repository)
    {
        $this->voucher_code_repository = $voucher_code_repository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->voucher_code_repository->checkOwnerByEmail(
            $this->route('voucher_code'),
            isset($this->email) ? $this->email : $this->route('email')
        );
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
