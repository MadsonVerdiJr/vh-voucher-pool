<?php

namespace App\Http\Requests;

use App\Repositories\RecipientRepositoryInterface;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RecipientUpdateRequest extends FormRequest
{

    /**
     * Recipient repository interface
     *
     * @var App\Repositories\RecipientRepositoryInterface
     */
    private $recipient_repository;

    /**
     * Class constructor
     *
     * @param RecipientRepositoryInterface $recipient_repository - Recipient repository interface
     */
    public function __construct(RecipientRepositoryInterface $recipient_repository)
    {
        $this->recipient_repository = $recipient_repository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $recipient = $this->recipient_repository->getById($this->route('recipient'));

        return [
            'name' => 'required|max:120',
            'email' => [
                'required',
                Rule::unique('recipients')->ignore($recipient->id),
            ],
        ];
    }
}
