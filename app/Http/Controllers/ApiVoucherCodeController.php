<?php

namespace App\Http\Controllers;

use App\VoucherCode;
use App\Http\Requests\ApiVoucherCodeRedeemRequest;
use App\Http\Resources\VoucherCodeCollection;
use App\Http\Resources\VoucherCodeResource;
use App\Repositories\VoucherCodeRepositoryInterface;
use Illuminate\Http\Request;

class ApiVoucherCodeController extends Controller
{
    /**
     * Voucher Code repository interface
     *
     * @var App\Repositories\VoucherCodeRepositoryInterface
     */
    private $voucher_code_respository;

    /**
     * Class constructor
     *
     * @param VoucherCodeRepositoryInterface $voucher_code_respository - Voucher Code repository interface
     */
    public function __construct(VoucherCodeRepositoryInterface $voucher_code_respository)
    {
        // Middlewares
        $this->middleware('route.filters')->only(['index']);

        // Repositories interfaces
        $this->voucher_code_respository = $voucher_code_respository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $voucher_codes = $this->voucher_code_respository->getAll($request->attributes->get('filters_array'));

        return new VoucherCodeCollection($voucher_codes);
    }

    /**
     * Redeem Voucher Code
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function redeem(ApiVoucherCodeRedeemRequest $request)
    {
        $voucher = $this->voucher_code_respository->setUsed($request->voucher_code);

        return new VoucherCodeResource($voucher);
    }
}
