<?php

namespace App\Http\Controllers;

use App\Http\Requests\RecipientCreateRequest;
use App\Http\Requests\RecipientUpdateRequest;
use App\Http\Resources\RecipientResource;
use App\Repositories\RecipientRepositoryInterface;
use App\Repositories\VoucherCodeRepositoryInterface;
use App\Repositories\SpecialOfferRepositoryInterface;
use Illuminate\Http\Request;

class RecipientController extends Controller
{
    /**
     * Recipient repository interface
     *
     * @var \App\Repositories\RecipientRepositoryInterface
     */
    private $recipient_repository;

    /**
     * Special Offer repository interface
     *
     * @var \App\Repositories\SpecialOfferRepositoryInterface
     */
    private $special_offer_repository;

    /**
     * Voucher Codes repository interface
     *
     * @var \App\Repositories\VoucherCodeRepositoryInterface
     */
    private $voucher_code_repository;

    /**
     * Class constructor
     *
     * @param \App\Repositories\RecipientRepositoryInterface    $recipient_repository       - Recipient repository interface
     * @param \App\Repositories\SpecialOfferRepositoryInterface $special_offer_repository   - Special Offer repository interface
     * @param \App\Repositories\VoucherCodeRepositoryInterface  $voucher_code_repository    - Voucher Code repository interface
     */
    public function __construct(
        RecipientRepositoryInterface $recipient_repository,
        SpecialOfferRepositoryInterface $special_offer_repository,
        VoucherCodeRepositoryInterface $voucher_code_repository
    ) {
        // Middlewares
        $this->middleware('route.filters')->only(['index']);

        // Repositories interfaces
        $this->recipient_repository     = $recipient_repository;
        $this->special_offer_repository = $special_offer_repository;
        $this->voucher_code_repository  = $voucher_code_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param \App\Http\Requests\RecipientCreateRequest  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $recipients = $this->recipient_repository->getAllPaginated($request->attributes->get('filters_array'));

        return view('recipient.index', compact('recipients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('recipient.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\RecipientCreateRequest  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(RecipientCreateRequest $request)
    {
        $recipient = $this->recipient_repository->create(
            $request->name,
            $request->email
        );

        return redirect()->route('recipient.show', ['recipient' => $recipient->slug]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $recipient      = $this->recipient_repository->getBySlug($request->recipient);
        $voucher_codes  = $this->voucher_code_repository->getLatestFromRecipient($recipient);
        $special_offers = $this->special_offer_repository->getLatestFromRecipient($recipient);

        return view('recipient.show', compact('recipient', 'voucher_codes', 'special_offers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $recipient = $this->recipient_repository->getBySlug($request->recipient);

        return view('recipient.edit', compact('recipient'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\RecipientUpdateRequest  $request
     * @param  int                                       $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(RecipientUpdateRequest $request, $id)
    {
        $recipient = $this->recipient_repository->update($id, $request->name, $request->email);

        return redirect()->route('recipient.show', $recipient->slug);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->recipient_repository->delete($id);

        return redirect()->route('recipient.index');
    }
}
