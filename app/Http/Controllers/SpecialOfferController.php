<?php

namespace App\Http\Controllers;

use App\Http\Requests\SpecialOfferRequest;
use App\Repositories\SpecialOfferRepositoryInterface;
use App\Repositories\RecipientRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SpecialOfferController extends Controller
{
    /**
     * Recipient repository interface
     *
     * @var \App\Repositories\RecipientRepositoryInterface
     */
    private $recipient_repository;

    /**
     * Special Offer repository interface
     *
     * @var App\Repositories\SpecialOfferRepositoryInterface
     */
    private $special_offer_repository;

    /**
     * Class constructor
     *
     * @param SpecialOfferRepositoryInterface $special_offer_repository - Special Offer respository interface
     */
    public function __construct(
        RecipientRepositoryInterface $recipient_repository,
        SpecialOfferRepositoryInterface $special_offer_repository
    ) {
        // Middlewares
        $this->middleware('route.filters')->only(['index', 'show']);

        // Repositories interfaces
        $this->recipient_repository = $recipient_repository;
        $this->special_offer_repository = $special_offer_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $special_offers = $this->special_offer_repository->getAllPaginated($request->attributes->get('filters_array'));

        return view('special-offer.index', compact('special_offers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('special-offer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\SpecialOfferRequest    $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(SpecialOfferRequest $request)
    {
        $special_offer = $this->special_offer_repository->create(
            $request->name,
            $request->discount,
            Carbon::createFromFormat('Y-m-d', $request->validity)
        );

        return redirect()->route('special-offer.show', $special_offer->slug);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $special_offer = $this->special_offer_repository->getBySlug($request->special_offer);
        $recipients = $this->recipient_repository->getAll($request->attributes->get('filters_array'));

        return view('special-offer.show', compact('special_offer', 'recipients'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $special_offer = $this->special_offer_repository->getBySlug($request->special_offer);

        return view('special-offer.edit', compact('special_offer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\SpecialOfferRequest    $request
     * @param  integer                                  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(SpecialOfferRequest $request, $id)
    {
        $special_offer = $this->special_offer_repository->update(
            $id,
            $request->name,
            $request->discount,
            Carbon::createFromFormat('Y-m-d', $request->validity)
        );

        return redirect()->route('special-offer.show', $special_offer->slug);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->special_offer_repository->delete($id);

        return redirect()->route('special-offer.index');
    }
}
