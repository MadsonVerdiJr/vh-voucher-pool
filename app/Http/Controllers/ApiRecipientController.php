<?php

namespace App\Http\Controllers;

use App\Http\Resources\RecipientResource;
use App\Repositories\RecipientRepositoryInterface;
use Illuminate\Http\Request;

class ApiRecipientController extends Controller
{
    /**
     * Recipient repository interface
     *
     * @var \App\Repositories\RecipientRepositoryInterface
     */
    private $recipient_repository;

    /**
     * Class constructor
     *
     * @param \App\Repositories\RecipientRepositoryInterface    $recipient_repository       - Recipient repository interface
     */
    public function __construct(RecipientRepositoryInterface $recipient_repository) {
        // Middlewares
        $this->middleware('route.filters')->only(['index']);

        // Repositories interfaces
        $this->recipient_repository = $recipient_repository;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $recipient = $this->recipient_repository->getByEmail($request->email);

        return new RecipientResource($recipient);
    }
}
