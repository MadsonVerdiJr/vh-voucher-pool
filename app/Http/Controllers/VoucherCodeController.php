<?php

namespace App\Http\Controllers;

use App\VoucherCode;
use App\Http\Requests\VoucherCodeRedeemRequest;
use App\Http\Resources\VoucherCodeCollection;
use App\Repositories\VoucherCodeRepositoryInterface;
use Illuminate\Http\Request;

class VoucherCodeController extends Controller
{
    /**
     * Voucher Code repository interface
     *
     * @var App\Repositories\VoucherCodeRepositoryInterface
     */
    private $voucher_code_respository;

    /**
     * Class constructor
     *
     * @param VoucherCodeRepositoryInterface $voucher_code_respository - Voucher Code repository interface
     */
    public function __construct(VoucherCodeRepositoryInterface $voucher_code_respository)
    {
        // Middlewares
        $this->middleware('route.filters')->only(['index']);

        // Repositories interfaces
        $this->voucher_code_respository = $voucher_code_respository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $voucher_codes = $this->voucher_code_respository
            ->getAllPaginated($request->attributes->get('filters_array'));


        return view('voucher-code.index', compact('voucher_codes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $voucher_code = $this->voucher_code_respository
            ->getByCode($request->voucher_code);

        return view('voucher-code.show', compact('voucher_code'));
    }

    /**
     * Redeem Code form page
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function redeem(Request $request)
    {
        $voucher_code = $this->voucher_code_respository->getByCode($request->voucher_code);

        return view('voucher-code.redeem', compact('voucher_code'));
    }

    public function processRedeem(VoucherCodeRedeemRequest $request)
    {
        $this->voucher_code_respository->setUsed($request->voucher_code);

        return redirect()->route('voucher-code.show', $request->voucher_code);
    }
}
