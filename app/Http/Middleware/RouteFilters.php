<?php

namespace App\Http\Middleware;

use Closure;

class RouteFilters
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $filters = [];

        ////////////////
        // Recipients //
        ////////////////
        if (isset($request->recipient)) {
            $filters['recipient_slug'] = $request->recipient;
        } else if (isset($request->recipient_id)) {
            $filters['recipient_id'] = $request->recipient_id;
        }

        if (isset($request->email)) {
            $filters['recipient_email'] = $request->email;
        }


        ////////////////////
        // Special Offers //
        ////////////////////
        if (isset($request->special_offer)) {
            $filters['special_offer_slug'] = $request->special_offer;
        } else if (isset($request->special_offer_id)) {
            $filters['special_offer_id'] = $special_offer_id;
        }

        /////////////
        // Generic //
        /////////////
        if (isset($request->name)) {
            $filters[$request->path().'_name'] = $request->name;
        }

        if (isset($request->limit)) {
            $filters['limit'] = $request->limit;
        } else {
            $filters['limit'] = 10;
        }

        $request->attributes->add(['filters_array' => $filters]);

        return $next($request);
    }
}
