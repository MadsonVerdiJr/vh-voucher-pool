<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VoucherCode extends Model
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'used',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'recipient_id',
        'special_offer_id',
        'code',
        'used',
    ];

    /**
     * Override primary key
     *
     * @var array
     */
    protected $primaryKey = [
        'recipient_id',
        'special_offer_id',
    ];

    /**
     * Override auto increment propertie
     *
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Recipient relationship
     *
     * @return \App\Recipient
     */
    public function recipient()
    {
        return $this->belongsTo(Recipient::class, 'recipient_id', 'id');
    }

    /**
     * Special Offer relationship
     *
     * @return \App\SpecialOffer
     */
    public function specialOffer()
    {
        return $this->belongsTo(SpecialOffer::class, 'special_offer_id', 'id');
    }

    public function scopeFilterEmail($query, $email)
    {
        return $query->whereHas('recipient', function ($recipient) use ($email) {
            $recipient->whereEmail($email);
        });
    }
}
