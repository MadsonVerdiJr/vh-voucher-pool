<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Blade::component('components.actionbar', 'actionbar');

        // Recipient Compoments
        // Blade::component('recipient.component.table', 'recipienttable');
        // Blade::component('recipient.component.card', 'recipientcard');

        // Voucher Code Components
        // Blade::component('voucher-code.component.table', 'vouchertable');

        // Special Offer Components
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
