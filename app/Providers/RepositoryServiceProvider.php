<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // Recipient
        $this->app->bind(
            'App\Repositories\RecipientRepositoryInterface',
            'App\Repositories\RecipientRepository'
        );

        // Special Offer
        $this->app->bind(
            'App\Repositories\SpecialOfferRepositoryInterface',
            'App\Repositories\SpecialOfferRepository'
        );

        // Voucher Code
        $this->app->bind(
            'App\Repositories\VoucherCodeRepositoryInterface',
            'App\Repositories\VoucherCodeRepository'
        );
    }
}
