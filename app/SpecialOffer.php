<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class SpecialOffer extends Model
{
    use Sluggable;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'validity',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'discount',
        'validity',
    ];

    /**
     * Get all recipients from the Special Offer
     *
     * @return \App\Recipient
     */
    public function recipients()
    {
        return $this->belongsToMany(
            Recipient::class,
            'voucher_codes',
            'special_offer_id',
            'recipient_id'
        );
    }
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ],
        ];
    }

    /**
     * Apply Name filter in model
     *
     * @param  \Illuminate\Database\Eloquent\Builde $query
     * @param  string                               $name
     *
     * @return \Illuminate\Database\Eloquent\Builde
     */
    public function scopeFilterName($query, $name)
    {
        return $query->where('name', 'like', '%'.$name.'%');
    }

    /**
     * Apply Name filter in model
     *
     * @param  \Illuminate\Database\Eloquent\Builde $query
     * @param  integer                              $name
     *
     * @return \Illuminate\Database\Eloquent\Builde
     */
    public function scopeFilterDiscountGreaterThan($query, $discount)
    {
        return $query->where('discount', '>=', $discount);
    }
}
