<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Recipient
Route::middleware('api')
    ->get('/recipient/{email}/', 'ApiRecipientController@show')
    ->name('api.recipient.show');

// Recipient vouchers
Route::middleware('api')
    ->get('/recipient/{email}/vouchers', 'ApiVoucherCodeController@index')
    ->name('api.recipient.show.vouchers');

// Redeem code
Route::middleware('api')
    ->get('/voucher-code/{voucher_code}/redeem/{email}/', 'ApiVoucherCodeController@redeem')
    ->name('api.voucher-code.redeem');
