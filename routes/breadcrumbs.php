<?php

// Home
Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push('Home', route('home'));
});

////////////////
// Recipients //
////////////////

// Home > Recipient
Breadcrumbs::register('recipient.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Recipient', route('recipient.index'));
});

// Home > Recipient > Create
Breadcrumbs::register('recipient.create', function ($breadcrumbs) {
    $breadcrumbs->parent('recipient.index');
    $breadcrumbs->push('Create', route('recipient.create'));
});

// Home > Recipient > Show
Breadcrumbs::register('recipient.show', function ($breadcrumbs, $recipient) {
    $breadcrumbs->parent('recipient.index');
    $breadcrumbs->push($recipient->name, route('recipient.show', $recipient->slug));
});

// Home > Recipient > Edit
Breadcrumbs::register('recipient.edit', function ($breadcrumbs, $recipient) {
    $breadcrumbs->parent('recipient.show', $recipient);
    $breadcrumbs->push('Edit', route('recipient.edit', $recipient->slug));
});

////////////////////
// Special Offers //
////////////////////

// Home > Special Offer
Breadcrumbs::register('special-offer.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Special Offer', route('special-offer.index'));
});

// Home > Special Offer > Create
Breadcrumbs::register('special-offer.create', function ($breadcrumbs) {
    $breadcrumbs->parent('special-offer.index');
    $breadcrumbs->push('Create', route('special-offer.create'));
});

// Home > Special Offer > Show
Breadcrumbs::register('special-offer.show', function ($breadcrumbs, $special_offer) {
    $breadcrumbs->parent('special-offer.index');
    $breadcrumbs->push($special_offer->name, route('special-offer.show', $special_offer->slug));
});

// Home > Special Offer > Show > Edit
Breadcrumbs::register('special-offer.edit', function ($breadcrumbs, $special_offer) {
    $breadcrumbs->parent('special-offer.show', $special_offer);
    $breadcrumbs->push('Edit', route('special-offer.edit', $special_offer->slug));
});


///////////////////
// Voucher codes //
///////////////////

// Home > Voucher Code
Breadcrumbs::register('voucher-code.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Voucher Code', route('voucher-code.index'));
});

// Home > Voucher Code > Show
Breadcrumbs::register('voucher-code.show', function ($breadcrumbs, $voucher_code) {
    $breadcrumbs->parent('voucher-code.index');
    $breadcrumbs->push($voucher_code->code, route('voucher-code.show', $voucher_code->code));
});

// Home > Voucher Code > Show > Redeem
Breadcrumbs::register('voucher-code.redeem', function ($breadcrumbs, $voucher_code) {
    $breadcrumbs->parent('voucher-code.show', $voucher_code);
    $breadcrumbs->push('Redeem', route('voucher-code.redeem', $voucher_code->code));
});

