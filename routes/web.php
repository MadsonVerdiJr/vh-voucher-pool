<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');


Route::resource('recipient', 'RecipientController');
Route::resource('special-offer', 'SpecialOfferController');
Route::resource('voucher-code', 'VoucherCodeController')->only(['index', 'show']);
Route::get('voucher-code/{voucher_code}/redeem', 'VoucherCodeController@redeem')->name('voucher-code.redeem');
Route::post('voucher-code/{voucher_code}/redeem', 'VoucherCodeController@processRedeem')->name('voucher-code.redeem');

